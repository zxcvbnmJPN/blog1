## セットアップ

```bash
# Install dependencies
yarn

# Run local server
yarn dev
```

ローカル環境:
[http://localhost:8080](http://localhost:8080)

### Netlifyを使用してホスティングしています。
本番環境:
[https://jovial-wright-87420d.netlify.com/](https://jovial-wright-87420d.netlify.com/)

### ビルドコマンド
yarn build:src

### ビルト先のディレクトリ
public/
